﻿namespace HaloSettingsCsvToAnalysisSettingsFiles
{
    using Microsoft.VisualBasic.FileIO;
    using System;
    using System.IO;

    public static class Program
    {
        private const int NameIndex = 0;
        private const int XmlIndex = 1;
        private const int StudyNameIndex = 2;
        private const int ModifiedTimeIndex = 3;

        public static void Main()
        {
            int rowCount = 0;
            try
            {
                var downloadsFolder = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                    "Downloads");
                var inputFile = Path.Combine(downloadsFolder, "settings.csv");
                var outputFolder = Path.Combine(downloadsFolder, "settings_files");
                Directory.CreateDirectory(outputFolder);
                using TextFieldParser parser = new TextFieldParser(inputFile)
                {
                    TextFieldType = FieldType.Delimited
                };
                parser.SetDelimiters("\t");
                while (!parser.EndOfData)
                {
                    var row = parser.ReadFields();
                    if (row.Length != ModifiedTimeIndex + 1)
                    {
                        throw new Exception($"Row {rowCount} has {row.Length} elements but should be exactly {ModifiedTimeIndex + 1}");
                    }
                    var name = row[NameIndex];
                    var xml = row[XmlIndex];
                    var studyName = row[StudyNameIndex];
                    var modifiedTime = DateTime.Parse(row[ModifiedTimeIndex]);
                    var filename = CleanFilename($"studyName_{studyName}_settingsName_{name}_settingsModified_{modifiedTime:yyyy-MM-dd_HH-mm-ss}.analysissettings");
                    var outputPath = Path.Combine(outputFolder, filename);
                    if (File.Exists(outputPath))
                    {
                        throw new Exception($"File {outputPath} already exists; duplicate name?");
                    }
                    File.WriteAllText(outputPath, xml);
                    rowCount++;
                }

                Console.WriteLine("Done, no errors (press any key to close)");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error (row {rowCount}): {ex.Message}");
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private static string CleanFilename(string s)
        {
            foreach (var invalid in Path.GetInvalidFileNameChars())
            {
                s = s.Replace(invalid, '_');
            }

            return s;
        }
    }
}
